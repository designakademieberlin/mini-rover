const POWER = 100

export class DrivingUnit {
  constructor(Gpio) {
    this.RTBackward = new Gpio(26, { mode: Gpio.OUTPUT });
    this.RTForward = new Gpio(16, { mode: Gpio.OUTPUT });
    this.LTBackward = new Gpio(17, { mode: Gpio.OUTPUT });
    this.LTForward = new Gpio(27, { mode: Gpio.OUTPUT });
    this.stop()
  }

  forward() {
    if (this.state !== 'forward') {
      this.stop()
      this.RTForward.pwmWrite(POWER);
      this.LTForward.pwmWrite(POWER);
      this.state = 'forward'
      console.log('forward')
    }
  }

  reverse() {
    if (this.state !== 'reverse') {
      this.stop()
      this.RTBackward.pwmWrite(POWER);
      this.LTBackward.pwmWrite(POWER);
      this.state = 'reverse'
      console.log('reverse')
    }
  }

  left() {
    if (this.state !== 'left') {
      this.stop()
      this.RTForward.pwmWrite(POWER + 50);
      this.LTBackward.pwmWrite(POWER + 50);
      this.state = 'left'
      console.log('left')
    }   
  }

  right() {
    if (this.state !== 'right') {
      this.stop()
      this.RTBackward.pwmWrite(POWER + 50);
      this.LTForward.pwmWrite(POWER + 50);
      this.state = 'right'
      console.log('right')
    }  
  }

  stop() {
    if (this.state !== 'idle') {
      this.RTForward.digitalWrite(0);
      this.LTForward.digitalWrite(0);
      this.RTBackward.digitalWrite(0);
      this.LTBackward.digitalWrite(0);  
      this.state = 'idle'
      console.log("stop");
    }   
  }
}

