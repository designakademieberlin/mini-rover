import SerialPort from 'serialport';
import GPS from 'gps';
import HMC5883L from 'compass-hmc5883l';
import { Gpio } from 'pigpio';
import { Server } from 'socket.io';
import onDeath from 'death';
import { DrivingUnit } from './utils/driving.js'

const drivingUnit = new DrivingUnit(Gpio)

let gpsPosition = null;

const COMPASS_BUS_NUMBER = 1;
const compass = new HMC5883L(COMPASS_BUS_NUMBER);

const socket = new Server({
  cors: {
    origin: '*'
  }
})
socket.listen(3000)

socket.on('connection', (client) => {
  client.on('mode', (mode) => {
    switch (mode) {
      case 'stop':
        drivingUnit.stop();
        break;
      case 'forward':
        drivingUnit.forward();
        break;
      case 'search':
        clearTimeout(interval)
        search()
    }
  })  
})

setInterval(() => {
  compass.getRawValues((err, values) => {
    socket.emit('magnetometerData', values)
    const { y } = values
    if (y !== 0) {
      socket.emit('mine', gpsPosition)
      clearTimeout(interval)
      drivingUnit.stop()
    }
  });
}, 200);

// SERACHING AN AREA
let i = 0
let interval = null

function search() {
  i = 0;
  nextStep()
}

function nextStep() {
  const driving = [
    { time: 4000, fn: drivingUnit.forward},
    { time: 1000, fn: drivingUnit.left},
    { time: 1000, fn: drivingUnit.forward },
    { time: 1000, fn: drivingUnit.left },
    { time: 4000, fn: drivingUnit.forward },
    { time: 1000, fn: drivingUnit.right},
    { time: 1000, fn: drivingUnit.forward },
    { time: 1000, fn: drivingUnit.right },
  ]
  driving[i].fn.call(drivingUnit)
  interval = setTimeout(() => {
    i++
    if (i >= 8) i = 0;
    nextStep()
  }, driving[i].time)
}

const sleep = (t) => new Promise((resolve) => setTimeout(() => resolve(undefined), t));

const port = new SerialPort('/dev/serial0', {
  baudRate: 9600,
});

const gps = new GPS();
const serial0 = port.pipe(new SerialPort.parsers.Readline({ delimiter: '\n' }));

serial0.on('data', (data) => {
  try {
    gps.update(data);
  } catch (e) {
    console.log(e);
  }
});

gps.on('data', (data) => {
  if (!data.valid) return;

  if (data.type === 'GGA' || data.type === 'GLL' || data.type === 'RMC') {
    gpsPosition = { time: data.time, nmeaType: data.type, lat: data.lat, lon: data.lon };
    socket.emit('position', gpsPosition)
  }
});

onDeath(() => {
  console.log('Resetting engines');
  drivingUnit.stop()

  process.exit(0);
});